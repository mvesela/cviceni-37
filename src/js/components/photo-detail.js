import { __addClass, __removeClass, __hasClass } from '../lib/utils';
import Component from './component';

// zoom the image
export default class PhotoDetail extends Component {
  constructor(element) {
    super(element);
    this.zoomIntensity = 3;
    this.targetContainer = this.target.parentElement;

    this._onClick = this._onClick.bind(this);
    this._onMove = this._onMove.bind(this);
    // TODO: if window size has changed
    this._onResize = this._onResize.bind(this);

    this._getImageDimensions();
    this._addEventListeners();
  }

  _getImageDimensions() {
    // returns the size of the scaled image
    // and its position relative to the viewport
    this.targetBCR = this.target.getBoundingClientRect();

    // we just need to set the height of the container
    // only for on window load and on window resize
    // width is 100% of its container
    this.targetContainer.style.height = `${this.targetBCR.height}px`;

    // TODO: count image side ratio
  }

  _onResize() {
    // returns the size of the scaled image
    // and its position relative to the viewport
    this.targetBCR = this.target.getBoundingClientRect();
  }

  _onClick(event) {
    if (event.target !== this.target) {
      return;
    }

    // Cancels the event if it is cancelable, without stopping further propagation of the event.
    event.preventDefault();

    // to zoom
    if (!__hasClass(this.target, 'enlarged')) {

      this._onResize();

      // add state class
      __addClass(this.target, 'enlarged');

      // get mouse event position
      this.x = event.clientX || event.touches[0].pageX;
      this.y = event.clientY || event.touches[0].pageY;

      // set position
      this.target.style.top = `${this.positionY}%`;
      this.target.style.left = `${this.positionX}%`;
    } else {
      // to unzoom
      // remove move state class if present
      if (__hasClass(this.target, 'detail-move')) {
        __removeClass(this.target, 'detail-move');
      }

      // remove zoom state class
      __removeClass(this.target, 'enlarged');

      // remove zoomed position
      this.target.style.left = '';
      this.target.style.top = '';
    }
  }

  _onMove(event) {
    if (event.target !== this.target || !__hasClass(this.target, 'enlarged')) {
      return;
    }

    // run only it was zoomed already
    if (__hasClass(this.target, 'enlarged')) {

      // add state class
      if (!__hasClass(this.target, 'detail-move')) {
        __addClass(this.target, 'detail-move');
      }

      // track mouse event position
      this.x = event.clientX || event.touches[0].pageX;
      this.y = event.clientY || event.touches[0].pageY;

      // get position
      this.target.style.top = `${this.positionY}%`;
      this.target.style.left = `${this.positionX}%`;
    }

  }

  static get positionY() {
    return (((this.y - this.targetBCR.top) / this.targetBCR.height) * -100) * (this.zoomIntensity - 1);
  }
  static get positionX() {
    return (((this.x - this.targetBCR.left) / this.targetBCR.width) * -100) * (this.zoomIntensity - 1);
  }

  _addEventListeners() {
    // touch events
    document.addEventListener('touchstart', this._onClick);
    document.addEventListener('touchmove', this._onMove);
    // mouse events
    document.addEventListener('mousedown', this._onClick);
    document.addEventListener('mousemove', this._onMove);
    // window events
    // window.addEventListener('resize', this._onResize);
  }
}
